# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.4.0]
### Fixed
- Funky Locomotion should no longer throw a server-killing null pointer exception on startup.

### Changed
- Pack now contains the entire FTB Inventions pack. This will make mod updates a little easier in future.
- Folded in updates from FTB Inventions v1.0.2.
- Witchery Rite of Nature's Power can now replace cracked sand.
- Rotarycraft tools no longer clear the chat when they're used.
- Rotarycraft machines should no longer be deafeningly loud.
- Stopped Tinkers' Construct loot from appearing in dungeon chests, as the items were badly broken. But fun. And fabulous.

## [1.3.0]
### Added
- zzzzz Custom Configs mod.

### Fixed
- Sesame seeds Agricraft crops now drop sesame seeds.
- Paging through achievements no longer crashes the game on the Applied Energistics 2 page.

### Changed
- Enabled freestanding ladders in Carpenter's Blocks.
- Increased smoothness of slopes in Carpenter's Blocks.
- Bedrock no longer generates flat.

## [1.2.0]
### Changed
- Upgraded v0.1.1 of Slack Bridge mod to v0.2.0 of JackActually's fork of same mod. This hides join/part messages and posts emotes from Minecraft to Slack.

## [1.1.0]
### Added
- Extra Utilities mod.
- Iguana's Tinker Tweaks mod.
- Slack Bridge mod.

### Changed
- Number of people required to be in bed to make sleep happen dropped to as low as possible.
- Waking message after sleep changed to something new and fun.
- Hunger bar depletion rate dropped to same as vanilla Minecraft.
- Hunger bar no longer depletes when player is not doing anything.
- Hunger bar emptying no longer causes instant death.

### Fixed
- Eating food no longer gives the flammable debuff.

## [1.0.0]
### Added
- Changelog file.
- Initial round of mods.
- Initial config tweaks for FTB Inventions and custom mods.