FTB Inventions was made to be as server/client friendly as possible, doing so resulted in a few changes to mods.  Below is a list of the changes made to increase performance.

-All chunkloading blocks/items are disabled, use FTBU to load chunks.  If you are playing on a server the admins set the amount of chunks, if any, you may force load.

-Immersive Engineering Bauxite ore (oreDict:oreAluminum) name changed to Aluminum Ore in NEI to prevent confusion from TechReborn Bauxite ore (oreDict:oreBauxite) both are required in world gen.  Waila may not reflect these changes.

Applied Energistics 2
-Meteorites are disabled.
-Presses are craftable, check NEI for recipes.
-All P2P components are removed from the mod.
-Craft CPU operations decreased from 5 to 1 per tick.

Big Reactors
-Fuel Usage Mutiplier set to 8.

Soul Shards
-Max number of Entities soul cages can spawn in an area decreased from 80 to 20.

MineFactory Reload
-Harverster block breaking sounds disabled.

ExtraBiomesXL
-Large tree biomes disabled except for an occasional Redwood Forrest Biome with Redwood Tress. (fast graphics is suggested if living in or near this biome)

Immersive Engineering
-Cables will only carry and transfer RF power.  Use Power Convertors mod if you need to convert RF to EU or EU to RF.

Logistics Pipes
-Disabled special particals from pipes.
-Pipe connection check times increased from 600 ticks to 1200 ticks to reduce CPU usage. (1 minute)

Project Red
-Minimum amount of ticks the timer gates can be set to increased from 4 ticks to 20 ticks (1 second)

Router Reborn
-Amount of ticks between each transfer of items increased from 2 to 5 (0.25 seconds)

Simply Jetpacks
-Tiers 1-4 jet plates sprint speed locked at 1.0
-Tier 5 jet plates sprint speed decreased from 2.4 to 1.3

