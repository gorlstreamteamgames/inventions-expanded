# README #

### What is this thing? ###

* This repository is an expansion pack for FTB Inventions, created for use by myself and a small group of good friends (you know who you are <3).
* Version

### How do I get set up? ###

1. Download FTB Inventions using the FTB Launcher.
2. Download this repository.
3. Copy FTBInventions into your Minecraft directory, overwriting any files that it prompts you to overwrite.

### Who do I talk to? ###

Any problems, contact jax on TTAS Slack.